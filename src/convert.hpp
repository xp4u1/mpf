#ifndef MPF_CONVERT_HPP
#define MPF_CONVERT_HPP

#include <string>

namespace mpf
{

bool fileExists(const std::string &filename);
void convert(const std::string &infile, std::string &outfile, std::string &topic);

} // namespace mpf

#endif //MPF_CONVERT_HPP
