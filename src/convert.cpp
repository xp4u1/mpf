#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <streambuf>
#include "convert.hpp"

const std::string header = "---\n"
                           "documentclass: extarticle\n"
                           "fontsize: 12pt\n"
                           "geometry: margin=2.54cm\n"
                           "header-includes: |\n"
                           "  \\usepackage[ngerman]{babel}\n"
                           "  \\usepackage[autostyle=true,german=quotes]{csquotes}\n"
                           "  \\usepackage{fancyhdr}\n"
                           "  \\pagestyle{fancy}\n"
                           "  \\fancyhf{}\n"
                           "  \\fancyhead[C]{%topic%}\n"
                           "  \\rfoot{Seite \\thepage}\n"
                           "  \\usepackage[bottom]{footmisc}\n"
                           "---\n\n";

void replaceAll(std::string &str, const std::string &from, const std::string &to)
{
  if (from.empty())
    return;
  size_t start_pos = 0;
  while ((start_pos = str.find(from, start_pos)) != std::string::npos)
  {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
}

std::string createMarkdown(const std::string &infile, const std::string &topic)
{
  std::string markdown = header;
  replaceAll(markdown, "%topic%", topic);

  std::ifstream input(infile);
  std::string inputContent((std::istreambuf_iterator<char>(input)),
                           std::istreambuf_iterator<char>());

  markdown.append(inputContent);

  return markdown;
}

bool mpf::fileExists(const std::string &filename)
{
  std::fstream file{};
  file.open(filename, std::ios_base::out | std::ios_base::in); // don't create a file

  return file.is_open();
}

void mpf::convert(const std::string &infile, std::string &outfile, std::string &topic)
{
  if (outfile == "")
  {
    outfile = infile;
    replaceAll(outfile, ".md", ".pdf");
  }
  if (topic == "")
  {
    topic = infile;
    replaceAll(topic, ".md", "");
  }

  std::string content = createMarkdown(infile, topic);

  std::ofstream markdownFile;
  markdownFile.open("pandoc.md");
  markdownFile << content;
  markdownFile.close();

  std::string pandocCommand = "pandoc -V geometry:a4paper -M csquotes=true -t latex -o " + outfile + " pandoc.md";
  system(pandocCommand.c_str());

  system("rm pandoc.md");
}
