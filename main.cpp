#include <iostream>
#include <clipp.hpp>
#include "src/convert.hpp"

int main(int argc, char *argv[])
{
    using namespace clipp;

    std::string infile, outfile, topic;

    auto cli = (value("input file", infile),
                option("-o", "--output") & value("output filename", outfile),
                option("-t", "--topic") & value("topic of the document", topic));

    if (parse(argc, argv, cli) && mpf::fileExists(infile))
    {
        std::cout << "Converting " << infile << "..." << '\n';
        mpf::convert(infile, outfile, topic);

        return 0;
    }

    std::cout << "                  __ \n"
                 " _ __ ___  _ __  / _|\n"
                 "| '_ ` _ \\| '_ \\| |_ \n"
                 "| | | | | | |_) |  _|\n"
                 "|_| |_| |_| .__/|_|  \n"
                 "          |_|        \n\n"
              << make_man_page(cli, argv[0])
                     .prepend_section("DESCRIPTION", "        Create cool Latex pdf-files from your markdown notes\n        Make sure you have installed pandoc!")
              << '\n';

    return 1;
}
