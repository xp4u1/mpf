Create beautiful latex pdf-files from your markdown notes without annoying yml headers and long pandoc commands.
Just install pandoc and mpf.

To create a simple page with `test` header write:

```sh
mpf test.md
```

To change the output file from `test.pdf` to `output.pdf` type:

```sh
mpf test.md -o output.pdf
```

You want a different topic?

```sh
mpf test.md -t topic
```

with whitespace:

```sh
mpf test.md -t your\ topic
```

```sh
mpf test.md -t "your topic"
